import home from './pages/home.vue';
import agent from './pages/agent.vue';

const rootRoute = {
  routes: [
    {
      path:'/home', name: 'home', component: home,
    },
    {
      path:'/agent',name: 'agent',component: agent
    }
  ],
  mode: 'history'
}

export default rootRoute;
