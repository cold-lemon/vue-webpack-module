import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import routerConfig from './router';

Vue.use(VueRouter);
const router = new VueRouter(routerConfig)
new Vue({
  el: '#app',
  router: router,
  template:'<App/>',
  components: { App },
  render: h => h(App)
})
// const app = new Vue({
//   router
// }).$mount('#app')
