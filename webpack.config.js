const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');  //打包vue
const HtmlWebpackPlugin = require('html-webpack-plugin');//打包html
const MiniCssExtractPlugin = require('mini-css-extract-plugin');//打包css

module.exports = {
  mode: 'development',
  entry: {
    main: "./src/index.js",
  },
  output: {
    path: path.resolve(__dirname, '/dist'),
    filename: "{name].js"
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.(png|svg|jpg|gif|PNG|JPG)$/,
        use: [
          'url-loader'
        ]
      },
      {
        test: /\.(css|less|sass)$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      },
      {
        test: '/\.js$/',
        loader: "babel-loader",
        exclude: /node_modules/
      }

    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./index.html",///指定要打包的html路径和文件名
      filename: "./index.html"//指定输出路径和文件名（相对js的路径）
    })
    ,
    // new MiniCssExtractPlugin({
    //   filename:"./"
    // }),
    new VueLoaderPlugin()
  ]
}
